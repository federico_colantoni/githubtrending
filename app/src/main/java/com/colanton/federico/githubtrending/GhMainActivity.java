package com.colanton.federico.githubtrending;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class GhMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
